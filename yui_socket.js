YUI().use('button','dial', function(Y){
  var socketStatus = document.getElementById('socketStatus');
  var socket = new WebSocket('ws://192.168.50.2/ws/');

  var potmeterdial = new Y.Dial({
    min:0,
    max:1,
    stepsPerRevolution:1,
    decimalPlaces:2,
    value: 0.5,
    handleDiameter: 0.3,
    strings:{label:'value:', resetStr: '', tooltipHandle: 'Drag to set'}
  });
  potmeterdial.render('#demo')


    // Handle any errors that occur.
    socket.onerror = function(error) {
      console.log('WebSocket Error: ' + error);
    };


    // Show a connected message when the WebSocket is opened.
    socket.onopen = function(event) {
      socketStatus.innerHTML = 'Connected to: ws://echo.websocket.org';
      socketStatus.className = 'open';
    };


    // Handle messages sent by the server.
    socket.onmessage = function(event) {
      var message = event.data;
      //console.log(Number(message));
      potmeterdial.set('value',Number(message));
    };


    // Show a disconnected message when the WebSocket is closed.
    socket.onclose = function(event) {
      socketStatus.innerHTML = 'Disconnected from WebSocket.';
      socketStatus.className = 'closed';
    };


  var LEDbutton = new Y.Button({
      srcNode:'#ToggleLED',
      on: {
          'click': function(){
              //var pressed = toggleButton.get('pressed');
              //toggleButton.set('pressed', !pressed);
              socket.send("floep\0");
              //console.log("floep..")
          }
      }
  }).render();




});
